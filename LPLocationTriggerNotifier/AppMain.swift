
import SwiftUI

@main
struct AppMain: App {
  @StateObject private var locationManager = LocationManager()

  var body: some Scene {
    WindowGroup {
      ContentView()
        .environmentObject(locationManager)
    }
  }
}
