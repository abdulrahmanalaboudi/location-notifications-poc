
import SwiftUI

struct ContentView: View {
  let takeOut = LocationsStore()
  @EnvironmentObject private var locationManager: LocationManager

  var body: some View {
    NavigationView {
      MenuListView(takeOut: takeOut)
    }
    .navigationViewStyle(StackNavigationViewStyle())
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
