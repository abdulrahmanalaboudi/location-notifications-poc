
import SwiftUI

struct DetailView: View {
  @EnvironmentObject private var locationManager: LocationManager
  @State private var orderPlaced = false
  let place: PlaceItem

  var body: some View {
    VStack {
      Image(place.imageName)
        .resizable()
        .frame(maxWidth: 300, maxHeight: 600)
        .aspectRatio(contentMode: .fit)
      Text(place.name)
        .font(.headline)
      Divider()
      Button(action: placeOrder) {
        Text("Remind me when reaching this location")
          .foregroundColor(.white)
          .frame(minWidth: 100, maxWidth: .infinity)
          .frame(height: 45)
      }
      .background(Color("rw-green"))
      .cornerRadius(3.0)
    }
    .alert(isPresented: $orderPlaced) {
      Alert(
        title: Text(""),
        message:
          Text("""
            Would you like to be notified on arrival?
            """),
        primaryButton: .default(Text("Yes")) {
          requestNotification()
        },
        secondaryButton: .default(Text("No"))
      )
    }
    .padding()
    .navigationBarTitle(Text(place.name), displayMode: .inline)
  }

  func placeOrder() {
    orderPlaced = true
  }

  func requestNotification() {
    locationManager.validateLocationAuthorizationStatus()
    locationManager.requestNotificationAuthorization(place: place)
  }
}

struct DetailView_Previews: PreviewProvider {
  static var previews: some View {
    DetailView(place: places[0])
  }
}
