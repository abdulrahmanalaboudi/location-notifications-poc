
import SwiftUI

struct MenuListRow: View {
  var menuItem: PlaceItem

  var body: some View {
    HStack {
      Image(menuItem.imageName)
        .resizable()
        .aspectRatio(contentMode: .fit)
        .frame(width: 50, height: 50)
      Text(menuItem.name)

      Spacer()
    }
  }
}

struct MenuListRow_Previews: PreviewProvider {
  static var previews: some View {
    MenuListRow(menuItem: places[0])
      .previewLayout(.fixed(width: 300, height: 50))
  }
}
