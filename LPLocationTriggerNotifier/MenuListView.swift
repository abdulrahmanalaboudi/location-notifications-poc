
import SwiftUI

struct MenuListView: View {
  var takeOut: LocationsStore
  var menuItems: [PlaceItem] {
    return takeOut.menu
  }

  var body: some View {
    List(menuItems, id: \.id) { item in
      NavigationLink(
        destination: DetailView(place: item)) {
        MenuListRow(menuItem: item)
      }
    }
    .navigationTitle(takeOut.title)
  }
}

struct MenuListView_Previews: PreviewProvider {
  static var previews: some View {
    MenuListView(takeOut: LocationsStore())
  }
}
