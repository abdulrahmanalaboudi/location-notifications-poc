
import CoreLocation
import UserNotifications

class LocationManager: NSObject, ObservableObject {
  
  lazy var locationManager = makeLocationManager()
  let notificationCenter = UNUserNotificationCenter.current()

  override init() {
    super.init()

    notificationCenter.delegate = self
  }

  private func makeLocationManager() -> CLLocationManager {
    let manager = CLLocationManager()
    return manager
  }
  
  private func makeStoreRegion(place: PlaceItem) -> CLCircularRegion {
    
    let location = CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude)

    let region = CLCircularRegion(
      center: location,
      radius: 2,
      identifier: UUID().uuidString)

    region.notifyOnEntry = place.notifyOnEntry
    region.notifyOnExit = place.notifyOnExit

    return region
  }
  
  func validateLocationAuthorizationStatus() {

    switch locationManager.authorizationStatus {

    case .notDetermined, .denied, .restricted:
      print("Location Services Not Authorized")
      
      locationManager.requestWhenInUseAuthorization()
    case .authorizedWhenInUse, .authorizedAlways:
      print("Location Services Authorized")
    default:
      break
    }
  }
  
  func requestNotificationAuthorization(place: PlaceItem) {
    let options: UNAuthorizationOptions = [.sound, .alert]

    notificationCenter
      .requestAuthorization(options: options) { [weak self] result, _ in

        print("Notification Auth Request result: \(result)")
        
        if result {
          self?.registerNotification(place: place)
        }
      }
  }

  private func registerNotification(place: PlaceItem) {

    let notificationContent = UNMutableNotificationContent()
    notificationContent.title = "You have reached \(place.name)"
    notificationContent.body = "Please click me to know more."
    notificationContent.sound = .default

    let placeRegion = makeStoreRegion(place: place)
    
    let trigger = UNLocationNotificationTrigger(
      region: placeRegion,
      repeats: true)

    let request = UNNotificationRequest(
      identifier: UUID().uuidString,
      content: notificationContent,
      trigger: trigger)

    notificationCenter
      .add(request) { error in
        if error != nil {
          print("Error: \(String(describing: error))")
        }
      }
  }
}

extension LocationManager: UNUserNotificationCenterDelegate {

  func userNotificationCenter(
    _ center: UNUserNotificationCenter,
    didReceive response: UNNotificationResponse,
    withCompletionHandler completionHandler: @escaping () -> Void
  ) {
    print("Received Notification")

    completionHandler()
  }

  func userNotificationCenter(
    _ center: UNUserNotificationCenter,
    willPresent notification: UNNotification,
    withCompletionHandler completionHandler:
      @escaping (UNNotificationPresentationOptions) -> Void
  ) {
    print("Received Notification in Foreground")

    completionHandler(.sound)
  }

}
