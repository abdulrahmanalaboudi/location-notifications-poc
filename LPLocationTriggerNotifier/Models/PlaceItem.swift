
import Foundation

struct PlaceItem {
  let id = UUID()
  var name: String
  var imageName: String
  var latitude: Double
  var longitude: Double
  var notifyOnEntry: Bool
  var notifyOnExit: Bool
}

// MARK: - Menu Items
let places: [PlaceItem] = [
  PlaceItem(name: "Waha Circle", imageName: "leading-point_icon", latitude: 31.991015966455546, longitude: 35.868784699767446, notifyOnEntry: true, notifyOnExit: true),
  PlaceItem(name: "Leading Point Software", imageName: "leading-point_icon", latitude: 31.98696597851745, longitude: 35.87883474024577, notifyOnEntry: true, notifyOnExit: true),
  PlaceItem(name: "Khalda", imageName: "leading-point_icon", latitude: 31.99670236584738, longitude: 35.85336626881593, notifyOnEntry: true, notifyOnExit: true),
  PlaceItem(name: "Home", imageName: "leading-point_icon", latitude: 31.993699, longitude: 35.852029, notifyOnEntry: true, notifyOnExit: true)
]


